from __future__ import print_function

import argparse
import os
import random
import sys

# import matplotlib.image as mpimg
import numpy as np
import tensorflow as tf
import hparamsMaker
import inference
import train
from seq2seqModel.utils import evaluation_utils
from seq2seqModel.utils import general_utils as utils
from seq2seqModel.utils import vocab_utils

utils.check_tensorflow_version()


def create_hparams(domain):
    settings = hparamsMaker.ModelSettings()
    argums = settings.get(domain=domain, id=id)
    hparams = tf.contrib.training.HParams(**argums['flags'])
    hparams = extend_hparams(hparams)
    return hparams



def extend_hparams(hparams):
    """Extend training hparams."""
    assert hparams.num_encoder_layers and hparams.num_decoder_layers
    if hparams.num_encoder_layers != hparams.num_decoder_layers:
        hparams.pass_hidden_state = False
        utils.print_out("""Num encoder layer {hparams.num_encoder_layers} is
                         different from num decoder layer
                         {hparams.num_decoder_layers}, so set
                         pass_hidden_state to False""")

    # Sanity checks
    if hparams.encoder_type == "bi" and hparams.num_encoder_layers % 2 != 0:
        raise ValueError("For bi, num_encoder_layers %d should be even" %
                         hparams.num_encoder_layers)

    # Set residual layers
    num_encoder_residual_layers = 0
    num_decoder_residual_layers = 0
    if hparams.residual:
        if hparams.num_encoder_layers > 1:
            num_encoder_residual_layers = hparams.num_encoder_layers - 1
        if hparams.num_decoder_layers > 1:
            num_decoder_residual_layers = hparams.num_decoder_layers - 1

    hparams.add_hparam("num_encoder_residual_layers",
                       num_encoder_residual_layers)
    hparams.add_hparam("num_decoder_residual_layers",
                       num_decoder_residual_layers)

    if hparams.subword_option and hparams.subword_option not in ["spm", "bpe"]:
        raise ValueError("subword option must be either spm, or bpe")

    # Flags
    utils.print_out("# hparams:")
    utils.print_out("  source=%s" % hparams.src)
    utils.print_out("  target=%s" % hparams.tgt)
    utils.print_out("  train_prefix=%s" % hparams.train_prefix)
    utils.print_out("  dev_prefix=%s" % hparams.dev_prefix)
    utils.print_out("  test_prefix=%s" % hparams.test_prefix)
    utils.print_out("  out_dir=%s" % hparams.out_dir)
    utils.print_out("  beam width size=%s" % hparams.beam_width)

    # Vocab
    # Get vocab file names first
    if hparams.vocab_prefix:
        src_vocab_file = hparams.vocab_prefix + "." + hparams.src
        tgt_vocab_file = hparams.vocab_prefix + "." + hparams.tgt
    else:
        raise ValueError("hparams.vocab_prefix must be provided.")

    # Source vocab
    src_vocab_size, src_vocab_file = vocab_utils.check_vocab(
        src_vocab_file,
        hparams.out_dir,
        check_special_token=hparams.check_special_token,
        sos=hparams.sos,
        eos=hparams.eos,
        unk=vocab_utils.UNK)

    # Target vocab
    if hparams.share_vocab:
        utils.print_out("  using source vocab for target")
        tgt_vocab_file = src_vocab_file
        tgt_vocab_size = src_vocab_size
    else:
        tgt_vocab_size, tgt_vocab_file = vocab_utils.check_vocab(
            tgt_vocab_file,
            hparams.out_dir,
            check_special_token=hparams.check_special_token,
            sos=hparams.sos,
            eos=hparams.eos,
            unk=vocab_utils.UNK)
    hparams.add_hparam("src_vocab_size", src_vocab_size)
    hparams.add_hparam("tgt_vocab_size", tgt_vocab_size)
    hparams.add_hparam("src_vocab_file", src_vocab_file)
    hparams.add_hparam("tgt_vocab_file", tgt_vocab_file)

    # Pretrained Embeddings:
    hparams.add_hparam("src_embed_file", "")

    hparams.add_hparam("tgt_embed_file", "")
    if hparams.embed_prefix:
        src_embed_file = hparams.embed_prefix + "." + hparams.src
        tgt_embed_file = hparams.embed_prefix + "." + hparams.tgt

        if tf.gfile.Exists(src_embed_file):
            hparams.src_embed_file = src_embed_file

        if tf.gfile.Exists(tgt_embed_file):
            hparams.tgt_embed_file = tgt_embed_file

    # Check out_dir
    if not tf.gfile.Exists(hparams.out_dir):
        utils.print_out("# Creating output directory %s ..." % hparams.out_dir)
        tf.gfile.MakeDirs(hparams.out_dir)

    # Evaluation
    for metric in hparams.metrics:
        hparams.add_hparam("best_" + metric, 0)  # larger is better
        best_metric_dir = os.path.join(hparams.out_dir, "best_" + metric)
        hparams.add_hparam("best_" + metric + "_dir", best_metric_dir)
        tf.gfile.MakeDirs(best_metric_dir)

        if hparams.avg_ckpts:
            hparams.add_hparam("avg_best_" + metric, 0)  # larger is better
            best_metric_dir = os.path.join(
                hparams.out_dir, "avg_best_" + metric)
            hparams.add_hparam("avg_best_" + metric + "_dir", best_metric_dir)
            tf.gfile.MakeDirs(best_metric_dir)

    return hparams

def start_inference(hparams, out_dir, num_workers, jobid):
    # Inference indices
    hparams.inference_indices = None
    if hparams.inference_list:
        hparams.inference_indices = [
            int(token)
            for token in hparams.inference_list
        ]

    # Inference
    trans_file = hparams.inference_output_file
    ckpt = hparams.ckpt
    if not ckpt:
        ckpt = tf.train.latest_checkpoint(out_dir)
    inference.inference(ckpt, hparams.inference_input_file,
                        trans_file, hparams, num_workers, jobid)

    # Evaluation
    ref_file = hparams.inference_ref_file
    if ref_file and tf.gfile.Exists(trans_file):
        for metric in hparams.metrics:
            score = evaluation_utils.evaluate(
                ref_file,
                trans_file,
                metric,
                hparams.subword_option)
            utils.print_out("  %s: %.1f" % (metric, score))


def main():
    """Run main."""

    from argparse import ArgumentParser

    def build_argument_parser():
        parser = ArgumentParser()
        model_group = parser.add_mutually_exclusive_group(required=True)
        model_group.add_argument('--id', type=int,
                                 help="select model by id (starting at 0)")
        model_group.add_argument('--domain', type=str,
                                 help="select model by name")

        runtype_group = parser.add_mutually_exclusive_group(required=True)
        runtype_group.add_argument('--train', help='do training',
                                   dest='training', action='store_true')
        runtype_group.add_argument('--decode', help='do inference',
                                   dest='inference', action='store_true')

        parser.add_argument('--input_file', type=str, help='file contains user messages to be decoded.')

        parser.add_argument('--no-preprocess', '-np', dest='preprocess',
                            action='store_false', help='disable dataset preprocessing')

        return parser.parse_known_args()

    args, unknown = build_argument_parser()
    print(args)
    hparams = create_hparams(args.domain)
    num_workers = hparams.num_workers
    random_seed = hparams.random_seed
    jobid = hparams.jobid
    out_dir = hparams.out_dir

    utils.print_out("# Job id %d" % jobid)
    if random_seed is not None and random_seed > 0:
        utils.print_out("# Set random seed to %d" % random_seed)
        random.seed(random_seed + jobid)
        np.random.seed(random_seed + jobid)

    if not tf.gfile.Exists(out_dir):
        tf.gfile.MakeDirs(out_dir)


    training = args.training
    inference = args.inference
    hparams.inference_input_file = args.input_file


    if inference:
        start_inference(hparams,out_dir, num_workers, jobid)
    elif training:
        train.run_train(hparams, target_session="")
if __name__=="__main__":
    main()

