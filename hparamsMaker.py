"""parse the default json file to generate model parameters"""

import json
import os
from typing import List
import tensorflow as tf




class ModelSettings():
    defaults: dict
    all_models: List[dict]

    def __init__(self):
        current_path = os.path.dirname(__file__)
        base = os.path.join(current_path,'config')
        with open(base+"/defaults.json") as fp:
            self.defaults = json.load(fp)

        with open(base+'/settings.json') as fp:
            self.all_models = json.load(fp)

    def get(self, domain:str = "", id: int =-1):
        setting = dict
        if domain:
            setting = self.__get_by_domain(domain)

        elif id != -1:
            setting = self.__get_by_id(id)

        else:
            raise ValueError('Please enter either "domain" or "id".')

        return {**setting, 'flags': self.__add_defaults(setting,key='flags')}

    def __add_defaults(self, setting: dict, key: str):
        return {
            **self.defaults[key],
            **setting[key]
        }

    def __get_by_domain(self,domain):
        item = [setting for setting in self.all_models if setting['domain'] == domain]
        if len(item) !=1:
            raise ValueError(f'Model name is not unique or not found.')

        return item[0]

    def __get_by_id(self, id):
        return self.all_models[id]

def test():
    import sys

    print(sys.path)
    settings = ModelSettings()
    args = settings.get(domain="crm", id=id)

    hparams = tf.contrib.training.HParams(**args['flags'])
    print(hparams.test_prefix)





#if __name__=="__main__":

test()
